package com.devcamp.voucherapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class VoucherApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VoucherApiApplication.class, args);
	}

}
