package com.devcamp.voucherapi.Services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.voucherapi.Models.Voucher;
import com.devcamp.voucherapi.Repository.VoucherRepository;

@Service
public class VoucherService {
    private final VoucherRepository voucherRepository;

    public VoucherService(VoucherRepository voucherRepository) {
        this.voucherRepository = voucherRepository;
    }

    public List<Voucher> getVouchers(String page, String size) {
        int pageNumber = Integer.parseInt(page);
        int pageSize = Integer.parseInt(size);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page<Voucher> pageVoucher = voucherRepository.findAll(pageable);
        return pageVoucher.getContent();
    }

    public Voucher getVoucherById(String id) {
        long voucherId = Long.parseLong(id);
        return voucherRepository.findById(voucherId).orElse(null);
    }

    public Voucher createVoucher(Voucher voucher) {
        return voucherRepository.save(voucher);
    }

    public Voucher updateVoucher(Voucher voucher, String id) {
        long voucherId = Long.parseLong(id);
        Voucher existingVoucher = voucherRepository.findById(voucherId).orElse(null);

        if (existingVoucher != null) {
            existingVoucher.setMaVoucher(voucher.getMaVoucher());
            existingVoucher.setPhanTramGiamGia(voucher.getPhanTramGiamGia());
            existingVoucher.setGhiChu(voucher.getGhiChu());

            return voucherRepository.save(existingVoucher);
        }
        return null;
    }

    public boolean deleteVoucher(String id) {
        long idValue = Long.parseLong(id);
        try {
            voucherRepository.deleteById(idValue);
            return true;
        } catch (Exception e) {

            return false;
        }
    }

    public boolean deleteAllVouchers() {
        try {
            voucherRepository.deleteAll();

            return true;
        } catch (Exception e) {

            return false;
        }
    }
}
