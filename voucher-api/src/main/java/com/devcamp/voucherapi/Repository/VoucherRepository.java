package com.devcamp.voucherapi.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.voucherapi.Models.Voucher;

import jakarta.transaction.Transactional;

public interface VoucherRepository extends JpaRepository<Voucher, Long> {
    @Query(value = "SELECT * FROM vouchers ORDER BY phan_tram_giam_gia DESC", nativeQuery = true)
    List<Voucher> getCVoucherDESC();

    @Transactional
    @Modifying
    @Query(value = "UPDATE vouchers SET phan_tram_giam_gia = :phanTram WHERE ma_voucher  = :ma_voucher", nativeQuery = true)
    int updatePhanTram(@Param("ma_voucher") String maVoucher, @Param("phanTram") String phanTram);

}
