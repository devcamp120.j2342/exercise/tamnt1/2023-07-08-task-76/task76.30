package com.devcamp.voucherapi.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.voucherapi.Models.Voucher;
import com.devcamp.voucherapi.Services.VoucherService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class VoucherController {
    @Autowired
    VoucherService voucherService;

    @PostMapping("/create-voucher")
    public ResponseEntity<Voucher> createVoucher(@RequestBody Voucher voucher) {
        try {
            Voucher newVoucher = new Voucher();
            newVoucher.setMaVoucher(voucher.getMaVoucher());
            newVoucher.setPhanTramGiamGia(voucher.getPhanTramGiamGia());
            newVoucher.setGhiChu(voucher.getGhiChu());

            Voucher createdVoucher = voucherService.createVoucher(newVoucher);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdVoucher);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

    @GetMapping("/voucher-list")
    public ResponseEntity<List<Voucher>> getVoucherList(@RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        try {
            List<Voucher> voucherList = voucherService.getVouchers(page, size);
            return ResponseEntity.ok(voucherList);
        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping("/vouchers/{id}")
    public ResponseEntity<Voucher> getVoucherList(@PathVariable(value = "id") String id) {
        try {
            Voucher voucher = voucherService.getVoucherById(id);
            return ResponseEntity.status(HttpStatus.OK).body(voucher);
        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PostMapping("/delete-voucher/{id}")
    public ResponseEntity<String> deleteVoucher(@PathVariable(value = "id") String id) {
        try {
            boolean isDeleted = voucherService.deleteVoucher(id);
            if (isDeleted) {
                return ResponseEntity.ok("Voucher deleted successfully.");
            } else {
                return ResponseEntity.ok("Failed to delete voucher.");
            }
        } catch (Exception e) {
            String errorMessage = "An error occurred while deleting the voucher: " + e.getMessage();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
        }
    }

    @PostMapping("/delete-vouchers")
    public ResponseEntity<String> deleteAllVouchers() {
        try {
            boolean isDeleted = voucherService.deleteAllVouchers();
            if (isDeleted) {
                return ResponseEntity.ok("Vouchers deleted successfully.");
            } else {
                return ResponseEntity.ok("Failed to delete vouchers.");
            }
        } catch (Exception e) {
            String errorMessage = "An error occurred while deleting vouchers: " + e.getMessage();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
        }
    }

    @PostMapping("/update-voucher/{id}")
    public ResponseEntity<Voucher> updateVoucher(@PathVariable(value = "id") String id, @RequestBody Voucher voucher) {
        try {

            Voucher updatedVoucher = voucherService.updateVoucher(voucher, id);
            return ResponseEntity.status(HttpStatus.OK).body(updatedVoucher);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
